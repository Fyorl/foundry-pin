'use strict';

const FPIN_RETRY_ATTEMPTS = 5;
const FPIN_RETRY_WAIT_MS = 1000;

function fpin_getState (entity) {
	if (!entity || !entity.collection) {
		return;
	}

	let id = entity.id;
	let owner = entity.actor;

	const pins = game.settings.get('foundry-pin', 'pins');
	const key = owner == null ? id : `${owner.id}.${id}`;
	let state = pins[key];

	if (!state) {
		state = {
			pinned: false,
			entity: entity.collection.documentName
		};
		pins[key] = state;
	}

	state.key = key;
	return state;
}

function fpin_saveState (state, app) {
	state.x = app.position.left;
	state.y = app.position.top;
	state.h = app.position.height;
	state.w = app.position.width;

	if (app._skycons !== undefined) {
		state.skycons = {};
		if (app._minimized) {
			state.skycons.minpos = {x: app.position.left, y: app.position.top};
		} else if (app._skycons.minpos !== undefined) {
			state.skycons.minpos = {x: app._skycons.minpos.x, y: app._skycons.minpos.y};
		}
	}

	const pins = game.settings.get('foundry-pin', 'pins');
	pins[state.key] = state;
	game.settings.set('foundry-pin', 'pins', pins);
}

Hooks.once('ready', () => {
	const style = $(`
		<style>
			.app.window-app.minimized .window-header > .pin {
				display: unset;
			}
		</style>
	`);

	document.head.appendChild(style[0]);

	game.settings.register('foundry-pin', 'pins', {
		name: 'pins',
		default: {},
		scope: 'client'
	});

	const pins = game.settings.get('foundry-pin', 'pins');
	Object.entries(pins).filter(([_, state]) => state.pinned).forEach(([key, state]) => {
		let id = key;
		let owner = null;

		if (id.includes('.')) {
			const split = id.split('.');
			owner = split[0];
			id = split[1];
		}

		let entity;
		if (owner == null) {
			entity = game.collections.get(state.entity)?.get(id);
		} else {
			const actor = game.actors.get(owner);
			if (actor) {
				entity = actor.getEmbeddedDocument('Item', id);
			}
		}

		if (entity && entity.sheet) {
			let retries = 0;
			const openSheet = async () => {
				try {
					let x = state.x;
					let y = state.y;

					if (state.skycons !== undefined && state.skycons.minpos !== undefined) {
						x = state.skycons.minpos.x;
						y = state.skycons.minpos.y;
					}

					await entity.sheet._render(true);
					await entity.sheet.minimize();
					entity.sheet.position.width = state.w;
					entity.sheet.position.height = state.h;
					entity.sheet.setPosition({top: y, left: x});

					if (state.skycons !== undefined) {
						entity.sheet._skycons = duplicate(state.skycons);
					}
				} catch {
					// There are some race conditions around how other mods
					// load here. I think the best we can do is just retry a
					// few times and hope things are loaded by then.
					retries++;
					entity.sheet._state = Application.RENDER_STATES.NONE;

					if (retries < FPIN_RETRY_ATTEMPTS) {
						setTimeout(openSheet, FPIN_RETRY_WAIT_MS);
					}
				}
			};

			openSheet();
		}
	});
});

Application.prototype._renderOuter = (function () {
	const cached = Application.prototype._renderOuter;
	return async function () {
		const html = await cached.apply(this, arguments);
		if (this.options.pinnable === false) {
			return html;
		}

		const state = fpin_getState(this.document);
		if (!state) {
			return html;
		}

		const pin = $('<a class="pin"><i class="fas fa-thumbtack"></i></a>');
		pin.insertBefore(html.find('a.close'));

		if (state.pinned) {
			pin.css('color', 'red');
		}

		pin.click(() => {
			state.pinned = !state.pinned;
			fpin_saveState(state, this);

			if (state.pinned) {
				pin.css('color', 'red');
			} else {
				pin.css('color', pin.next().css('color'));
			}
		});

		return html;
	};
})();

Application.prototype.minimize = (function () {
	return function () {
		if (!this.popOut || [true, null].includes(this._minimized) || !this.element.length) {
			return;
		}

		const window = this.element;
		const header = window.find('.window-header');

		window.css({
			minWidth: 100,
			minHeight: 30,
			height: `${header[0].offsetHeight + 1}px`,
			width: MIN_WINDOW_WIDTH
		}).addClass('minimized');

		this._minimized = true;
	};
})();

Application.prototype._onResize = (function () {
	const cached = Application.prototype._onResize;
	return function () {
		cached.apply(this, arguments);
		const pins = game.settings.get('foundry-pin', 'pins');
		const state = fpin_getState(this.document, pins);

		if (!state) {
			return;
		}

		state.w = this.position.width;
		state.h = this.position.height;
		game.settings.set('foundry-pin', 'pins', pins);
	};
})();

Draggable.prototype._onDragMouseUp = (function () {
	const cached = Draggable.prototype._onDragMouseUp;
	return function () {
		cached.apply(this, arguments);
		if (!this.app || this.app.options.pinnable === false) {
			return;
		}

		const pins = game.settings.get('foundry-pin', 'pins');
		const state = fpin_getState(this.app.document, pins);
		if (!state) {
			return;
		}

		fpin_saveState(state, this.app);
	};
})();
